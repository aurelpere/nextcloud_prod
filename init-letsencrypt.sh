#!/bin/bash
set -ex
if ! [ -x "$(command -v docker compose)" ]; then
  echo 'Error: docker compose is not installed.' >&2
  exit 1
fi
user=almalinux
domaine=nextcloud.alternatiba47.com
domains=($domaine)
rsa_key_size=4096
data_path="./certbot"
email="aurel.pere@gmail.com" # Adding a valid address is strongly recommended
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

#if [ -d "$data_path" ]; then
#  read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
#  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
#    exit
#  fi
#fi


if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  sudo mkdir -p "$data_path/conf"
  sudo curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  sudo curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
  echo
fi

if [ ! -d "/home/$user/$domaine" ]; then
	echo "### Creating dummy certificate for $domains ..."
	path="/etc/letsencrypt/live/$domains"
	mkdir -p "$data_path/conf/live/$domains"
	sudo docker compose run --rm --entrypoint "\
  	  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
   	  -keyout '$path/privkey.pem' \
      -out '$path/fullchain.pem' \
      -subj '/CN=localhost'" certbot
	echo
	#debug sudo docker compose run --rm --entrypoint "openssl req -x509 -nodes -newkey rsa:4096 -days 1 -keyout '/etc/letsencrypt/live/nextcloud.alternatiba47.com/privkey.pem' -out '/etc/letsencrypt/live/nextcloud.alternatiba47.com/fullchain.pem' -subj '/CN=localhost'" certbot
else
	sudo mkdir -p /app/certbot/conf/live/$domaine
	sudo cp -r /home/$user/$domaine /app/certbot/conf/live/
	sudo chown -R $user /app/certbot/conf/live/
fi
    
echo "### Starting nginx ..."
sudo docker compose up --force-recreate -d nginx
echo

if [ ! -d "/home/$user/$domaine" ]; then
	echo "### Deleting dummy certificate for $domains ..."
	docker compose run --rm --entrypoint "\
    	rm -Rf /etc/letsencrypt/live/$domains && \
		rm -Rf /etc/letsencrypt/archive/$domains && \
		rm -Rf /etc/letsencrypt/renewal/$domains.conf" certbot
	echo
fi

#Join $domains to -d args
domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

if [ ! -d "/home/$user/$domaine" ]; then
	if sudo docker compose run --rm --entrypoint "\
  		certbot certonly --non-interactive --webroot -w /var/www/certbot \
    	$staging_arg \
    	$email_arg \
    	$domain_args \
    	--rsa-key-size $rsa_key_size \
    	--agree-tos \
    	--force-renewal" certbot ; then
		echo "### Let's Encrypt certificate fetched for $domains ..."
		sudo mkdir -p /home/$user/$domaine
		sudo cp -rL ./certbot/conf/live/$domaine /home/$user/
	fi
fi
#debug sudo docker compose run --rm --entrypoint "certbot certonly --non-interactive --webroot -w /var/www/certbot --staging --email aurel.pere@gmail.com -d nextcloud.alternatiba47.com --rsa-key-size 4096 --agree-tos --force-renewal" certbot

echo "### Reloading nginx ..."
sleep 10
sudo docker compose exec -T nginx nginx -s reload
echo "nginx reloaded"
