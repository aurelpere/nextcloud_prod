# nextcloud


## Utilisation

nextcloud prod server

Changer les variables docker dans Docker-env-variables/db.env

Renseigner les variables ansible dans group_vars (var_domain et var_app_path)

Renseigner les variables d'environnement gitlab LOGIN, SMTP_PASSWORD, SUDO_PASSWORD, SSH_PRIVATE_KEY

Renseigner l'ip dans ip.txt

Le git push déploie nextcloud sur le serveur avec ansible et récupere un certificat tls avec un script bash (à améliorer en format ansible pour la v2)


